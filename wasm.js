const sdkInit = require('./sdkInit');
const fs = require('fs');
const { sdk, user2, user3, user4, Utils: utils } = sdkInit();
const abiFile = './testData/evmContract/evm.json';
const contractFilePath = './testData/wasm/rust-fact-2.0.0.wasm';
const contractName = 'ledger';
const contractVersion = 'v1.0.0';
const runtimeType = 'WASMER';

async function demo() {
  console.log('------------------strart demo------------------');
  console.log('contract source code: https://git.chainmaker.org.cn/contracts/contracts-rust/-/tree/master/fact')
  console.log('------------------install contract------------------');
  var response = await sdk.userContractMgr.createUserContract({
    contractName,
    contractVersion,
    runtimeType,
    contractFilePath,
    userInfoList: [user2, user3, user4],
    params: {},
    withSyncResult: true
  });
  console.log(JSON.stringify(response, 4, 1));
  await sleep(2)
  await subscribeEvent(contractName);
  await subscribeBlock();
  await subscribeTx();
  console.log('------------------invoke contract save------------------');
  var response = await sdk.callUserContract.invokeUserContract({
    contractName, method: 'save', params: {
      file_hash: '1234567890',
      file_name: 'test.txt',
      time: Date.now() / 1000 | 0,
    },
    withSyncResult: true,
  });
  console.log(JSON.stringify(response, 4, 1));
  console.log('------------------query contract find_by_file_hash------------------');
  response = await sdk.callUserContract.queryContract({
    contractName, method: 'find_by_file_hash', params: {
      file_hash: '1234567890',
    },
  });
  console.log(JSON.stringify(response,4 ,1));
  console.log('------------------end demo------------------');
}

async function subscribeEvent(contractName) {
  const topic = 'topic_vx';
  const response = await sdk.subscribe.subscribeContractEvent(
    topic,
    contractName, (data, err) => {
      console.log(`!!!!!!!!!!!!!!!!!!!receive event start: ${contractName}!!!!!!!!!!!!!!!!!!!`);
      console.log(JSON.stringify(data, 4, 1));
      console.log(`!!!!!!!!!!!!!!!!!!!receive event stop: ${contractName}!!!!!!!!!!!!!!!!!!!`);
    },
  );
  return response;
}

async function subscribeBlock() {
  const response = await sdk.subscribe.subscribeBlock(1, 2, false, true, (blockHeader, err) => {
    console.log(`!!!!!!!!!!!!!!!!!!!receive block start!!!!!!!!!!!!!!!!!!!`);
    console.log(JSON.stringify(blockHeader))
    console.log(`!!!!!!!!!!!!!!!!!!!receive block stop!!!!!!!!!!!!!!!!!!!`);
  });
  return response;
}

async function subscribeTx() {
  const response = await sdk.subscribe.subscribeTx(0, -1, contractName, [], (tx, err) => {
    console.log(`!!!!!!!!!!!!!!!!!!!receive tx start!!!!!!!!!!!!!!!!!!!`);
    console.log(JSON.stringify(tx))
    console.log(`!!!!!!!!!!!!!!!!!!!receive tx stop!!!!!!!!!!!!!!!!!!!`);
  });
  return response;
}

async function sleep(second) {
  return new Promise((resolve) => {
    setTimeout(async () => {
      resolve();
    }, second * 1000);
  });
}

async function main() {
  try {
    await demo();
  } catch (err) {
    console.log(err);
  }
}

main()
