const sdkInit = require('./sdkInit');
const fs = require('fs');
const { sdk, user2, user3, user4, Utils: utils } = sdkInit();
const abiFile = './testData/evmContract/evm.json';
const contractFilePath = './testData/evmContract/bin';
const contractName = 'ledger';
const version = 'v1.0.0';
const runTimeType = 'EVM';

// param参数是一个数组，需要按照合约中的参数顺序构建数组，例如合约方法方法参数是transfer(address to, int value), 那么这里的参数就写成这样[utils.getEvmAddress(证书内容), 10]
// 获取证书地址的方法是utils.getEvmAddress(证书内容)
// 证书内容可以用fs.readFileSync(证书路径)，读取
// 路径需要传入绝对路径
// 调用evm合约
async function invokeEvm(contractName, method, param, abiFilePath) {
  const abi = require(abiFilePath);
  const encoded = utils.encodeABI(method, param, abi);
  const response = await sdk.callUserContract.invokeUserContract({
    contractName: utils.getContractAddress(contractName),
    method: encoded.slice(0, 8),
    params: {
      data: encoded,
    },
    withSyncResult: true,
  });
  const res = utils.decodeABI(method, abi, Buffer.from(response.contractResult.result, 'base64'));
  console.log(res.toString());
  return response;
}

// user 是一个user类的对象
async function invokeEvmByUser(contractName, method, param, abiFilePath, user) {
  const abi = require(abiFilePath);
  const encoded = utils.encodeABI(method, param, abi);
  const response = await sdk.callUserContract.invokeUserContractByUser({
    contractName: utils.getContractAddress(contractName),
    method: encoded.slice(0, 8),
    params: {
      data: encoded,
    },
    withSyncResult: true,
    user,
  });
  const res = utils.decodeABI(method, abi, Buffer.from(response.contractResult.result, 'base64'));
  console.log(res.toString());
  return response;
}

// 这里的param同上
// 查询evm合约
async function queryEvm(contractName, method, param, abiFilePath) {
  const abi = require(abiFilePath);
  const encoded = utils.encodeABI(method, param, abi);
  const response = await sdk.callUserContract.queryContract({
    contractName: utils.getContractAddress(contractName),
    method: encoded.slice(0, 8),
    params: {
      data: encoded,
    },
  });
  const res = utils.decodeABI(method, abi, Buffer.from(response.contractResult.result, 'base64'));
  console.log(res.toString());
  return response;
}

async function demo() {
  const address1 = utils.getEvmAddress(sdk.userInfo.userSignCertBytes);
  const address2 = utils.getEvmAddress(user2.userSignCertBytes);
  console.log('------------------strart demo------------------');
  console.log('------------------install contract------------------');
  const encoded = utils.encodeABI('', [], require(abiFile), true);
  console.log(await sdk.userContractMgr.createUserContract({
    contractName: utils.getContractAddress(contractName),
    contractVersion: version,
    runtimeType: runTimeType,
    contractFilePath: contractFilePath,
    userInfoList: [user2, user3, user4],
    params: {
      data: encoded
    },
    withSyncResult: true
  }));
  await sleep(2)
  await subscribeEvent(contractName);
  console.log('------------------invoke contract mint------------------');
  await invokeEvm(contractName, 'updateMyBalance', ["100"], abiFile);
  await invokeEvm(contractName, 'updateBalance', ["100", address2], abiFile);
  console.log('------------------query contract balances------------------');
  await queryEvm(contractName, 'balances', [address1], abiFile);
  await queryEvm(contractName, 'balances', [address2], abiFile);
  console.log('------------------invoke contract transfer------------------');
  await invokeEvm(contractName, 'transfer', [address2, '1'], abiFile);
  console.log('------------------query contract balances------------------');
  await queryEvm(contractName, 'balances', [address1], abiFile);
  await queryEvm(contractName, 'balances', [address2], abiFile);
  console.log('------------------end demo------------------');
}

async function subscribeEvent(contractName) {
  const contractEventName = 'Transfer(address,address,uint256)';
  const eventName = 'Transfer';
  const response = await sdk.subscribe.subscribeContractEvent(
    utils.getEventAddress(contractEventName),
    utils.getContractAddress(contractName), (ct, err) => {
      console.log(`!!!!!!!!!!!!!!!!!!!receive event start: ${contractEventName}!!!!!!!!!!!!!!!!!!!`);
      const event0 = ct.contractEventsList[0];
      const data = event0.eventDataList;
      const res = utils.decodeEventABI(eventName, require(abiFile), data);
      console.log(res.toString());
      console.log(`!!!!!!!!!!!!!!!!!!!receive event stop: ${contractEventName}!!!!!!!!!!!!!!!!!!!`);
    },
  );
  return response;
}

async function sleep(second) {
  return new Promise((resolve) => {
    setTimeout(async () => {
      resolve();
    }, second * 1000);
  });
}

async function main() {
  try {
    await demo();
  } catch (err) {
    console.log(err);
  }
}

main()
