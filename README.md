# NodeJS SDK demo

### 1、下载nodejs sdk到本地

```shell
git clone https://git.chainmaker.org.cn/chainmaker/sdk-nodejs.git
git checkout -b v2.2.0_evm tags/v2.2.0_evm
```

### 2、进入nodejs sdk根目录，执行

```shell
npm install
npm link
```

> 注意：依赖包@chilkat/ck-node14-linux64默认操作系统为linux64，node版本为14，如果想使用其他操作系统或者更高的node版本，需要替换该依赖包（package.json与src/utils/index.js中的依赖），npm链接：https://www.npmjs.com/search?q=%40chilkat%2Fck-node14

### 3、进入该项目根目录，执行

```shell
npm link nodejs-sdk
```

### 4、修改配置，配置所在位置为sdkInit.js文件中，修改证书、节点信息、用户信息、组织ID、链信息

> 注意：由chainmaker提供的脚本本地生成的链，不需要修改该文件，直接用chainmaker-go项目中生成的build/crypto-config文件夹替换testData/crypto-config文件夹即可，如果链不在本地部署，需要修改节点ip地址

### 5、演示evm合约

```shell
npm run evm
```

### 6、演示wasm合约

```shell
npm run wasm
```

> 注意：wasm合约与其他非evm和约部署和调用方式完全相同，修改合约文件、及运行时、调用方法以及调用参数即可，docker-go合约需要根据版本修改调用方法和参数，具体参考官方文档：https://docs.chainmaker.org.cn/v2.2.0/html/index.html

